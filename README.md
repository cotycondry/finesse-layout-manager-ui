# Finesse Layout Manager UI
This is the UI companion of the Finesse Layout Manager API service (https://gitlab.com/cotycondry/finesse-layout-manager-api.git)

## Requirements
  * Node >= v5
  * NPM >= v3
  * Webpack v2

## Installation
```sh
git clone https://gitlab.com/cotycondry/finesse-layout-manager-ui.git
cd finesse-layout-manager-ui
npm install
npm run build
```

## Run
Copy files to your www/html folder (or wherever you have your web server pointing to).
```sh
sudo cp finesse-layout-manager-ui/dist/* /var/www/html/
```
