import Vue from 'vue'
import Router from 'vue-router'
// import menuModule from 'vuex-store/modules/menu'
Vue.use(Router)

export default new Router({
  mode: 'hash', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'is-active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      name: 'Finesse Layout Manager',
      path: '/',
      component: require('../views/Finesse-Layout-Manager')
    },
    {
      name: 'ECE Gadget',
      path: '/gadgets/ece',
      component: require('../views/gadgets/ECE')
    },
    {
      name: 'Solve Gadget',
      path: '/gadgets/solve',
      component: require('../views/gadgets/Solve')
    },
    {
      name: 'SMS Gadget',
      path: '/gadgets/sms',
      component: require('../views/gadgets/SMS')
    },
    {
      name: 'Training Gadget',
      path: '/gadgets/training',
      component: require('../views/gadgets/Training')
    },
    {
      name: 'Login',
      path: '/login',
      component: require('../views/auth/Login')
    },
    // ...generateRoutesFromMenu(menuModule.state.items),
    {
      path: '*',
      redirect: '/'
    }
  ]
})

// Menu should have 2 levels.
// function generateRoutesFromMenu (menu = [], routes = []) {
//   for (let i = 0, l = menu.length; i < l; i++) {
//     let item = menu[i]
//     if (item.path) {
//       routes.push(item)
//     }
//     if (!item.component) {
//       generateRoutesFromMenu(item.children, routes)
//     }
//   }
//   return routes
// }
