import * as types from './mutation-types'

export const toggleSidebar = ({ commit }, data) => {
  if (data instanceof Object) {
    commit(types.TOGGLE_SIDEBAR, data)
  }
}

export const toggleDevice = ({ commit }, data) => commit(types.TOGGLE_DEVICE, data)

export const expandMenu = ({ commit }, data) => {
  if (data) {
    data.expanded = data.expanded || false
    commit(types.EXPAND_MENU, data)
  }
}

export const switchEffect = ({ commit }, data) => {
  if (data) {
    commit(types.SWITCH_EFFECT, data)
  }
}

export const setAuthToken = ({ commit }, data) => {
  if (data) {
    commit(types.SET_AUTH_TOKEN, data)
    // set authToken in localStorage also
    window.localStorage.setItem('authToken', data)
  }
}

export const setUsername = ({ commit }, data) => {
  if (data) {
    commit(types.SET_USERNAME, data)
    // set username in localStorage also
    window.localStorage.setItem('username', data)
  }
}

export const logout = ({ commit }) => {
  // remove auth token from localStorage and state
  window.localStorage.removeItem('authToken')
  commit(types.SET_AUTH_TOKEN, null)
  // remove username from localStorage and state
  window.localStorage.removeItem('username')
  commit(types.SET_USERNAME, null)

  // TODO tell server logout
}

export const checkLogin = ({ commit, rootState }) => {

}
