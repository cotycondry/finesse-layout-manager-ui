const pkg = state => state.pkg
const app = state => state.app
const device = state => state.app.device
const sidebar = state => state.app.sidebar
const effect = state => state.app.effect
const menuitems = state => state.menu.items
const componententry = state => {
  return state.menu.items.filter(c => c.meta && c.meta.label === 'Components')[0]
}
const apiBase = state => state.app.apiBase
const authenticated = state => state.app.authToken !== null
const authToken = state => state.app.authToken
const username = state => state.app.username

export {
  pkg,
  app,
  device,
  sidebar,
  effect,
  menuitems,
  componententry,
  apiBase,
  authToken,
  authenticated,
  username
}
