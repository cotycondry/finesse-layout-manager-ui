import * as types from '../mutation-types'

var apiBase = {
  'production': '/api',
  'development': 'http://localhost:3000/api'
}

const state = {
  apiBase: apiBase[process.env.NODE_ENV],
  authToken: null,
  username: null,
  device: {
    isMobile: false,
    isTablet: false
  },
  sidebar: {
    opened: false,
    hidden: false
  },
  effect: {
    translate3d: true
  }
}

const mutations = {
  [types.TOGGLE_DEVICE] (state, device) {
    state.device.isMobile = device === 'mobile'
    state.device.isTablet = device === 'tablet'
  },

  [types.TOGGLE_SIDEBAR] (state, config) {
    if (state.device.isMobile && config.hasOwnProperty('opened')) {
      state.sidebar.opened = config.opened
    } else {
      state.sidebar.opened = true
    }

    if (config.hasOwnProperty('hidden')) {
      state.sidebar.hidden = config.hidden
    }
  },

  [types.SWITCH_EFFECT] (state, effectItem) {
    for (let name in effectItem) {
      state.effect[name] = effectItem[name]
    }
  },

  [types.SET_AUTH_TOKEN] (state, data) {
    state.authToken = data
  },

  [types.SET_USERNAME] (state, data) {
    state.username = data
  }
}

export default {
  state,
  mutations
}
