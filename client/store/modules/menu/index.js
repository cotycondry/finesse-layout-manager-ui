import * as types from '../../mutation-types'
import lazyLoading from './lazyLoading'
import gadgets from './gadgets.js'

const state = {
  items: [
    {
      name: 'Finesse Layout',
      path: '/',
      meta: {
        icon: 'fa-fire',
        description: 'Code editor component that based on brace',
        link: 'finesse/layout.vue'
      },
      component: lazyLoading('Finesse-Layout-Manager')
    },
    gadgets
  ]
}

const mutations = {
  [types.EXPAND_MENU] (state, menuItem) {
    if (menuItem.index > -1) {
      if (state.items[menuItem.index] && state.items[menuItem.index].meta) {
        state.items[menuItem.index].meta.expanded = menuItem.expanded
      }
    } else if (menuItem.item && 'expanded' in menuItem.item.meta) {
      menuItem.item.meta.expanded = menuItem.expanded
    }
  }
}

export default {
  state,
  mutations
}
