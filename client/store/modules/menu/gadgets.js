import lazyLoading from './lazyLoading'

export default {
  name: 'Gadgets',
  path: '/gadgets',
  meta: {
    icon: 'fa-folder',
    expanded: true
    // link: 'gadgets/index.vue'
  },
  // component: lazyLoading('gadgets', true),

  children: [
    {
      name: 'ECE Gadget',
      path: '/gadgets/ece',
      meta: {
        link: 'finesse/gadgets/ECE.vue'
      },
      component: lazyLoading('Finesse-Layout-Manager')
    },
    {
      name: 'Solve Gadget',
      path: '/gadgets/solve',
      meta: {
        link: 'finesse/gadgets/Solve.vue'
      },
      component: lazyLoading('gadget-solve')
    },
    {
      name: 'SMS Gadget',
      path: '/gadgets/sms',
      meta: {
        link: 'finesse/gadgets/SMS.vue'
      },
      component: lazyLoading('gadget-solve')
    },
    {
      name: 'Training Gadget',
      path: '/gadgets/training',
      meta: {
        link: 'finesse/gadgets/Training.vue'
      },
      component: lazyLoading('gadget-training')
    }
  ]
}
